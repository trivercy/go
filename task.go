// Package main is a task.txt solution
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"time"
)

type itemJSON struct {
	LastName  string `json:"Last name"`
	FirstName string `json:"First name"`
	Age       string
	Gender    string
	Marital   string
	LastLogin string `json:"Last login"`
}

type Person struct {
	LastName  string
	FirstName string
	Age       int
	Gender    string
	Marital   bool
	LastLogin int
}

type byAge []*Person

func (x byAge) Len() int {
	return len(x)
}
func (x byAge) Less(i, j int) bool {
	return x[i].Age < x[j].Age
}
func (x byAge) Swap(i, j int) {
	x[i], x[j] = x[j], x[i]
}

func main() {
	// 1 Взять с адреса "http://82.196.1.83:9570" JSON
	data, err := getJSON("http://82.196.1.83:9570")
	if err != nil {
		fmt.Println("getJSON:", err)
		return
	}

	// 2 Распарсить записав в модель
	var items []itemJSON
	err = json.Unmarshal(data, &items)
	if err != nil {
		fmt.Println("Unmarshal:", err)
		return
	}
	//	fmt.Printf("%+v", items)

	// 3 Дополнить модель соблюдая типы:
	//		- числа должны быть int
	//		- дата в формате Unix
	//		- bool в формате bool
	var people = []*Person{}
	for _, i := range items {
		age, _ := strconv.Atoi(i.Age)
		t, _ := time.Parse("02.01.2006 15:04", i.LastLogin)
		m, _ := strconv.ParseBool(i.Marital)
		people = append(people, &Person{i.LastName, i.FirstName, age, i.Gender, m, int(t.Unix())})
	}
	//for _, p := range people {
	//	fmt.Printf("%+v\n", *p)
	//}

	// 4 Вывести в консоль отсортированные (по возрасту) данные:
	//		- в порядке убывания
	//		- в порядке возрастания
	fmt.Println("\nSort by Age DESC")
	sort.Sort(sort.Reverse(byAge(people)))
	for _, p := range people {
		fmt.Printf("%+v\n", *p)
	}
	fmt.Println("\nSort by Age ASC")
	sort.Sort(byAge(people))
	for _, p := range people {
		fmt.Printf("%+v\n", *p)
	}
	fmt.Println()

	// 5 Собрать данные в массив ключ->ключ->массив значений (`гендерная принадлежность` -> `семейное положение (bool - обязательно)` -> `люди`)
	res := make(map[string]map[string][]Person)
	for _, p := range people {
		m := res["m"]
		if m == nil {
			m = make(map[string][]Person)
			res["m"] = m
		}
		f := res["f"]
		if f == nil {
			f = make(map[string][]Person)
			res["f"] = f
		}

		mar := strconv.FormatBool(p.Marital)
		if p.Gender == "m" {
			group := m[mar]
			if group == nil {
				group = []Person{}
			}
			group = append(group, Person(*p))
			res["m"][mar] = group
		} else {
			group := f[mar]
			if group == nil {
				group = []Person{}
			}
			group = append(group, Person(*p))
			res["f"][mar] = group
		}
	}

	// 6 Вывести собраный массив ключ->ключ->массив значений в консоль
	fmt.Println("\nMap of key->key->slice")
	fmt.Printf("%+v\n", res)
}

func getJSON(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}
